#!/usr/bin/env python

import os
import pandas as pd

"""
################################################################################
ARCHIVO DE CONFIGURACION
################################################################################
"""

"""
# Setear el direcorio de salida de la informacion
"""
pathtofolder = "."
# Carpeta de los datos y figuras
outdir       = f"{pathtofolder}/data"
os.makedirs(outdir, exist_ok = True)
# carpeta temporal donde se descargan los datos (version paralelizada)
tmpdir       = f"{outdir}/tmp"
os.makedirs(tmpdir, exist_ok = True)

"""
# Fechas de interes, no se recomienda mas de 2 o 3 dias.
"""
date1   = "2023-04-24"      # Fecha incial
date2   = "2023-04-25"      # Fecha final

"""
# Seleccion MANUAL de EMAs zona S. C. Bariloche
# Fijarse en https://www.wunderground.com/wundermap?lat=-41.109&lon=-71.448
# Podria ser cualquier estacion no solo de bariloche
"""
estacion  = ["IDEPAR123", "IDEPAR138", "IBARIL3", "ISANCARL18", "IBARILOC6", "IBARILOC2", "ISANCARL19", "IDEPAR37", "ILOSLAGO6"]
elevacion = [  1620,    1437,    869,    775,    836,    859,    780,    932,    863]
latitud   = [-41.10,  -41.14, -41.13, -41.11, -41.08, -41.14, -41.13, -41.17, -41.00]
longitud  = [-71.57,  -71.38, -71.13, -71.42, -71.53, -71.32, -71.27, -71.37, -71.16]

################################################################################
################################################################################

# Diccionario: informacion
info    = {"estacion": estacion, "elevacion":elevacion, "latitud":latitud, "longitud":longitud}
# Lista de fechas
dates     = pd.date_range(date1, date2, freq='D')
