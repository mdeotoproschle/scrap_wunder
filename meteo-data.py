#!/usr/bin/env python

import os
import numpy as np
import pandas as pd
import matplotlib.pyplot as plt
from matplotlib.dates import DayLocator, DateFormatter, HourLocator
from matplotlib.ticker import MultipleLocator, FormatStrFormatter
import scrape_wunderground as scrape                            # Modulo de scrape
from config_file import tmpdir, outdir, info, dates                     # Archivo de configuracion


"""
##################################################################################
Este modulo descarga la información de las EMAs desde el sitio Weather Underground

IMPORTANTE: En el archivo de configuracion config_file.py se encuentran la
opcion de fechas y de estaciones meteorologicas

##################################################################################
"""

################################################################################
# DESCARGO Y GUARDO LOS DATOS. VER ARCHIVO CONFIG_FILE
################################################################################
dataframe       = scrape.multidown(info, dates, tmpdir, outdir)
dataframe.index = pd.to_datetime(dataframe.index, format = '%Y-%m-%d %H:%M')
################################################################################
# Figura
################################################################################
fig, ax = plt.subplots(figsize=(10, 8), layout='constrained')

for st in info["estacion"]:
    T    = dataframe["Temperature"][dataframe["estacion"]==st]
    elev = dataframe["elevacion"][dataframe["estacion"]==st]
    cs = ax.scatter(dataframe[dataframe["estacion"]==st].index, T, c=elev, cmap="jet", label=f"{st:10s} {elev[0]:5d} m".expandtabs(), vmin=800, vmax=1400)

#ax.set_ylim([-1,22])
ax.set_ylabel('Temperatura (°C)', fontsize=15)
ax.set_title(f"Estaciones meteorológicas zona bariloche\n{dates[0]:%Y%m%d} - {(dates[1] + pd.to_timedelta(1,'d')):%Y%m%d}", fontsize=15)     # Add a title to the axes.
ax.yaxis.set_major_locator(MultipleLocator(1.00))
ax.yaxis.set_minor_locator(MultipleLocator(0.5))
ax.xaxis.set_major_locator(DayLocator(interval = 1))
ax.xaxis.set_minor_locator(HourLocator(interval = 2))
ax.set_xlim(dates[0], dates[1] + pd.to_timedelta(1,'d'))
ax.xaxis.set_major_formatter(DateFormatter('%d/%m\n%Y'))
ax.xaxis.set_minor_formatter(DateFormatter('%H'))
ax.yaxis.set_tick_params(labelsize = 10)
ax.xaxis.set_tick_params(labelsize = 10)
ax.grid(True, which = "major", axis = "both", ls = "-", lw = 0.3, c = "0.5")
ax.grid(True, which = "minor", axis = "both", ls = "-", lw = 0.2, c = "0.75")
ax.set_axisbelow(True)
ax.tick_params(which = 'major', width = 0.25, length = 10)
ax.tick_params(which = 'minor', width = 0.15, length = 6)
ax.legend()                     # Add a legend.

cbar = plt.colorbar(cs)
cbar.set_label('Altura [m]', rotation=90, fontsize=14)

#plt.show()
plt.savefig(f'{outdir}/plot_temp.png', bbox_inches='tight')
