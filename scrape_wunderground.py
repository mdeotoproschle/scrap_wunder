#!/usr/bin/env python

import time, os, sys, glob, multiprocessing
import numpy as np
import pandas as pd
from bs4 import BeautifulSoup as BS
from selenium import webdriver
from selenium.webdriver.support.wait import WebDriverWait
from selenium.webdriver.support import expected_conditions as EC
from selenium.webdriver.common.by import By
from selenium.webdriver import FirefoxOptions
from functools import partial

def render_page(url):
    """Given a url, render it with firefox driver and return the html source Zach Perzan, 2021-07-28
    Parameters
    ----------
        url : url page
    Returns
    -------
        r :
            rendered page source
    """
    ###############################
    # Firefox webdriver [Matias De Oto, 2023]
    #driver = webdriver.Firefox()
    # Firefox webdriver [Matias De Oto, 2023]
    opts = FirefoxOptions()
    opts.add_argument('--headless')
    driver = webdriver.Firefox(options=opts)
    ###############################
    driver.get(url)
    #time.sleep(3) # Could potentially decrease the sleep time
    # Add this lines to get temperature in Celsius degrees [Matias De Oto, 2023]
    # From source code of Bojan Stavrikj [https://bojanstavrikj.github.io/content/page1/wunderground_scraper_celsius]
    #element = WebDriverWait(driver, 10).until(EC.presence_of_element_located((By.ID, 'wuSettings')))
    #element.click()
    #element = WebDriverWait(driver, 10).until(EC.presence_of_element_located((By.XPATH, '//*[@id="wuSettings-quick"]/div/a[2]')))
    #element.click()
    ###############################
    time.sleep(3)
    r = driver.page_source
    driver.quit()
    return r


def scrape_wunderground(station, date):
    """Given a PWS station ID and date, scrape that day's data from Weather Underground and return it as a dataframe.
    Parameters
    ----------
        station : str
            The personal weather station ID
        date : str
            The date for which to acquire data, formatted as 'YYYY-MM-DD'
    Returns
    -------
        df : dataframe or None
            A dataframe of weather observations, with index as pd.DateTimeIndex
            and columns as the observed data
    """
    # Render the url and open the page source as BS object
    url = 'https://www.wunderground.com/dashboard/pws/%s/table/%s/%s/daily' % (station, date, date)
    r = render_page(url)
    soup = BS(r, "html.parser",)
    container = soup.find('lib-history-table')
    # Check that lib-history-table is found
    if container is None:
        raise ValueError("could not find lib-history-table in html source for %s" % url)
    # Get the timestamps and data from two separate 'tbody' tags
    all_checks = container.find_all('tbody')
    time_check = all_checks[0]
    data_check = all_checks[1]
    # Iterate through 'tr' tags and get the timestamps
    hours = []
    for i in time_check.find_all('tr'):
        trial = i.get_text()
        hours.append(trial)
    ##########################
    # Adition line
    # De Oto, 2023
    # Iterate through strong to get wind direction
    windir = []
    for i in data_check.find_all('strong'):
        tito = i.get_text()
        windir.append(tito)
    windir = CompassToDeg(np.array(windir[1::4]),type="degrees")
    #########################
    # For data, locate both value and no-value ("--") classes
    classes = ['wu-value wu-value-to', 'wu-unit-no-value ng-star-inserted']
    # Iterate through span tags and get data
    data = []
    for i in data_check.find_all('span', class_=classes):
        trial = i.get_text()
        data.append(trial)

    ##############################
    # Cambio de float a string problema en Presion (involucra punto y coma) [Matias De Oto, 2023]
    columns = ['Temperature', 'Dew Point', 'Humidity', 'Wind Direction', 'Wind Speed', 'Wind Gust', 'Pressure', 'Precip. Rate', 'Precip. Accum.']
    # Convert NaN values (stings of '--') to np.nan
    data_nan = [np.nan if x == '--' else x for x in data]
    data_array = np.array(data_nan, dtype=float)
    #data_array  = np.array(data_nan, dtype=str)
    data_array = data_array.reshape(-1, len(columns)-1)
    data_array = np.concatenate([data_array, np.array(windir).reshape(-1, 1)], axis=1)
    # Rearange
    data_array = data_array[:, [0,1,2,-1,3,4,5,6,7]]
    ##############################

    # columns = ['Temperature', 'Dew Point', 'Humidity', 'Wind Speed', 'Wind Gust', 'Pressure', 'Precip. Rate', 'Precip. Accum.']
    # # Convert NaN values (stings of '--') to np.nan
    # data_nan = [np.nan if x == '--' else x for x in data]
    # ##############################
    # # Cambio de float a string problema en Presion (involucra punto y coma) [Matias De Oto, 2023]
    # data_array = np.array(data_nan, dtype=str)  #data_array = np.array(data_nan, dtype=float)
    # ##############################
    # data_array = data_array.reshape(-1, len(columns))

    # Prepend date to HH:MM strings
    timestamps = ['%s %s' % (date, t) for t in hours]
    # Convert to dataframe
    df = pd.DataFrame(index=timestamps, data=data_array, columns=columns)
    df.index = pd.to_datetime(df.index)
    ############
    # Conversions
    ############
    df['Temperature']    = (df['Temperature'] - 32) * 5/9
    df['Dew Point']      = (df['Dew Point'] - 32) * 5/9
    df['Wind Speed']     = df['Wind Speed'] * 0.868976
    df['Wind Gust']      = df['Wind Gust'] * 0.868976
    df['Pressure']       = df['Pressure'] * 33.8639
    df['Precip. Rate']   = df['Precip. Rate'] * 25.4
    df['Precip. Accum.'] = df['Precip. Accum.'] * 25.4

    return df


def scrape_multiattempt(station, date, attempts=4, wait_time=5.0):
    """Try to scrape data from Weather Underground. If there is an error on the
    first attempt, try again.
    Parameters
    ----------
        station : str
            The personal weather station ID
        date : str
            The date for which to acquire data, formatted as 'YYYY-MM-DD'
        attempts : int, default 4
            Maximum number of times to try accessing before failuer
        wait_time : float, default 5.0
            Amount of time to wait in between attempts
    Returns
    -------
        df : dataframe or None
            A dataframe of weather observations, with index as pd.DateTimeIndex
            and columns as the observed data
    """
    # Try to download data limited number of attempts
    for n in range(attempts):
        try:
            df = scrape_wunderground(station, date)
        except:
            # if unsuccessful, pause and retry
            time.sleep(wait_time)
        else:
            # if successful, then break
            break
    # If all attempts failed, return empty df
    else:
        df = pd.DataFrame()
    return df

################################################################################
# Function: Replace cardinal direction to degree direction
################################################################################
def CompassToDeg(wind_dir,type):
    wind_dir_copy = wind_dir#.copy(deep=True)
    sector  = ["","North","NNE","NE","ENE","East","ESE", "SE", "SSE","South","SSW","SW","WSW","West","WNW","NW","NNW"]
    degrees = [np.nan, 0, 22.5, 45, 67.5, 90, 112.5, 135, 157.5, 180, 202.5, 225, 247.5, 270, 292.5, 315, 337.5]
    if type == "degrees":
        for n,i in zip(sector,degrees):
            wind_dir_copy[wind_dir == n] = i
    elif type == "radians":
        radians = np.array(degrees)*np.pi/180
        for n,i in zip(sector,radians):
            wind_dir_copy[wind_dir == n] = i
    else:
        print("check 'type' variable")
        exit(1)
    return wind_dir_copy.astype(float)

################################################################################
# Opcion de descargar los archivos secuencialmente
################################################################################
def download(info,dates):
    # Proceso de descarga de la informacion
    dataframe = []
    for i, st in enumerate(info["estacion"]):
      for dts in dates:
        try:
          df = scrape_wunderground(st, f"{dts:%Y-%m-%d}")
          #df = scrape_multiattempt(st, date1, date2, attempts=4, wait_time=5.0) # descomento si hay congestion en la red
          # Creo nuevas columnas con la metadata de la estacion
          df["estacion"]  = info["estacion"][i]
          df["elevacion"] = info["elevacion"][i]
          df["latitud"]   = info["latitud"][i]
          df["longitud"]  = info["longitud"][i]
          # concatenacion loop
          dataframe.append(df)
          print(f'Ok estacion {st} y fecha {dts}')
        except:
          print(f'Error en la lectura de la estacion {st} o la fechas {dts}')
    # Concatenacion de la informacion
    dataframe = pd.concat(dataframe)
    return dataframe

################################################################################
## Opcion de descargar los archivos paralelizadamente
################################################################################
def download_par(tmpdir, info, dts, st):
    try:
      df = scrape_wunderground(st, f"{dts:%Y-%m-%d}")
      # Creo nuevas columnas con la metadata de la estacion
      df["estacion"]  = st
      df["elevacion"] = np.array(info["elevacion"])[np.array(info["estacion"])  == st][0]
      df["latitud"]   = np.array(info["latitud"])[np.array(info["estacion"])  == st][0]
      df["longitud"]  = np.array(info["longitud"])[np.array(info["estacion"])  == st][0]
      df.to_csv(f"{tmpdir}/{st}_{dts:%Y%m%d}.csv", sep=",")
      print(f'Ok estacion {st} y fecha {dts:%Y%m%d}')
    except:
      print(f'Error en la lectura de la estacion {st} o la fechas {dts:%Y%m%d}')

def multidown(info, dates, tmpdir, outdir):
    for dts in dates:
        pool    = multiprocessing.Pool(processes=len(info["estacion"]))
        func    = partial(download_par, tmpdir, info, dts)
        result  = pool.map(func, info["estacion"])
    # Los archivos se descargaron individualmente, ahora los abro y los leo
    all_files = glob.glob(os.path.join(tmpdir, "*.csv"))
    dataframe = pd.concat((pd.read_csv(f, index_col=0) for f in all_files)) #, ignore_index=True)
    # Opcion de guardar la informacion
    dataframe.to_csv(f"{outdir}/meteobari_{dates[0]:%Y%m%d}-{(dates[1] + pd.to_timedelta(1,'d')):%Y%m%d}.csv", sep=",", float_format="%.1f")
    return dataframe
