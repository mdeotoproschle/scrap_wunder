# Descarga de observaciones meteorologicas horarias desde wunderground
# Plus gráfico de temperatura


## Introducción

El [web-scraping](https://es.wikipedia.org/wiki/Web_scraping) es una técnica de programación ampliamente utilizada para obtener información de dominios web, en general privados, con múltiples propósitos. Como sabemos [Wunderground](https://www.wunderground.com/) tiene la capacidad de automatizar las Estaciones Meteorológicas Automáticas (EMAs) de privados en tiempo real con múltiples beneficios en el acceso de la información. Lamentablemente, esta información es de visualización en tiempo real, por lo que realizar un estudio en tiempo diferido ya sea investigación o actividades recreativas es arancelado. En este caso, utilizaremos el web-scraping para descargar información de EMAs cercanas a la región de S. C. de Bariloche de forma no-arancelada. A fin de cuentas, el web-scraping nos permite comprender el tiempo meteorológico de nuestra ciudad/región. La técnica de web-scrapping utilizada en el código de descarga se ha tomado de distintas bibliografías, de las que podemos citar a [**Zach Perzan (2021)**](https://zperzan.github.io/projects/scrape-weather-underground/) y a [**Bojan Stavrikj**](https://bojanstavrikj.github.io/content/page1/wunderground_scraper)


## Repositorio de Datos

Los información meteorológica se encuentra en resolución temporal de 5-minutos dentro de la base de datos de Weather Underground. Para acceder a la misma de forma automática realizamos el siguiente tutorial. Esencialmente, se diseñaron dos opciones. La opción 1 es mediante una computadora personal en la que se deberán descargar todas las libreráas necesarias para la ejecución del modulo de web-scraping. En la opción 2, se brinda un archivo **.ipynb** para la ejecución en una computadora virtual de Google-colab.


## Opcion 1

Es la opción más compleja porque debés tener algunos conocimientos mayores de software y algo de tiempo para "renegar" con la instalación de las librerias en tu propia PC, pero... si conoces bien a tu PC es la opción más viable a largo plazo. Para ello, es importante que tengas un entorno de trabajo conda en donde puedas tener Python y las librerias requeridas para la ejecución del código de web-scrapping, también algunos conocimientos básicos de cómo funciona un Git.

Si no sabés lo que es un entorno de trabajo conda te recomiendo leer este [tutorial](https://conda.io/projects/conda/en/latest/user-guide/tasks/manage-environments.html#creating-an-environment-with-commands) para que puedas crearte el tuyo en tu propia PC, y en él te instales la version de Python requerida y todas las librerias necesarias, incluyendo el Git.

### Requerimientos para PC personal

    * Sistema operativo: Ubuntu/Linux
    * Lenguaje de programación: Python versión +3.8
    * Librerías de python requeridas: bs4, selenium, numpy, pandas, matplotlib  

Continene los siguientes módulos

      1. **scrape_wunderground.py** código principal de web-scraping
      2. **meteo-data.py**          descarga la información meteorológica y grafica la temperatura.

En el módulo de descarga **meteo-data.py** ya estan definidas las EMAs de la región, sin embargo las fechas para la descarga de la información se deben insertar a mano

Asumiendo que tienes el Python y las librerias necesarias, incluyendo el Git, entonces te generás un directorio y abrís una terminal y copias el repositorio con todas las funciones de la siguiente forma:

    $ `git clone https://gitlab.com/mdeotoproschle/scrap_wunder.git`

### Ejecución

Una vez chequeado que las librerias estén bien instaladas. Desde la terminal de linux, posicionados en la carpeta donde se han descargado los archivos desde el git, ejecutas:

            $ `python meteo-data.py 2>log.err >log.out &`

**ATENCION**
El módulo **scrape_wunderground.py** debe estar en el directorio donde se ejecutará el archivo **meteo-data.py** en su defecto se debe setear el path donde se encuentra alojado mediante las siguientes líneas de código en el script **meteo-data.py**

            $ `import sys`
            $ `sys.path.append('path-to-module:scrape_wunderground.py')`

Si la ejecución resulta satisfactoria, en tu directorio de trabajo se debería generar una carpeta llamada tmp con un archivo **.csv** con la información meteorológica. Este archivo luego lo puedes utilizar para trabajarlo con cualquier otro lenguaje de programación. También se debería generar un archivo de imagen **.png** con el gráfico de la temperatura y unos archivos de errores de ejecución como las capturas de pantalla denominados log.err y log.out en el directorio de ejecución.


## Opcion 2

Como sabemos tu computadora tiene programas instalados que la diferencian de la computadora de tu vecino/a, esto hace que si yo te paso un código a vos para que lo ejecutes probablemente tengas que actualizarle o downgradearle los softwares a tu PC, bueno... Google-colab evita esto. Google-colab ofrece una máquina virtual compatible mundialmente por lo que yo podría pasarte un código generado en esa maquina virtual para que vos lo ejecutes en esa máquina virtual sin problemas. Ese código presenta la extension **.ipynb**. Para tener acceso a dicha máquina virtual sólo debés contar con una cuenta en Google. Pero si este resumen no te convenció acerca de qué es Google-colab,te recomiendo leer este [tutorial](https://colab.research.google.com/).


### Ejecución

Sube el archivo **.ipynb** a tu Nube de Google y ejecutalo todo de una vez o paso por paso, para ello les dejo las siguientes instrucciones:
instrucciones:

    1. Bajen el archivo “.ipynb” del git a sus computadoras (para bajarlo, apreten el boton derecho del mouse sobre el link, y elijan la opción para bajar el archivo)

    2. Vayan a https://colab.research.google.com

    3. Vayan a la pestaña “Subir (Upload)” y suban el archivo “.ipynb”.



# Agradecimientos

La contibución a este proyecto fue posible gracias a las contribuciones de [Bojan Stavrikj](https://bojanstavrikj.github.io/index) y de [Zach Perzan](https://zperzan.github.io/).
